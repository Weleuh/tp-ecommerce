addEventListener("load", () => {
    const productList = document.getElementById("productList")
    const nomCroissant = document.getElementById("A-Z")
    const nomDecroissant = document.getElementById("Z-A")
    const prixCroissant = document.getElementById("prixCroissant")
    const prixDecroissant = document.getElementById("prixDecroissant")



    fetch('../db.en.json')
        .then(response => response.json())
        .then(data => {
            console.log(data);
            updateList(data);
            nomCroissant.addEventListener("click", () => {
                data.sort((a, b) => {
                    const nomA = a.nom.toLowerCase();
                    const nomB = b.nom.toLowerCase();

                    if (nomA < nomB) return -1;
                    if (nomA > nomB) return 1;
                    return 0;
                });
                console.log(data);
                productList.innerHTML = "";
                updateList(data)
            })
            nomDecroissant.addEventListener("click", () => {
                data.sort((a, b) => {
                    const nomA = a.nom.toLowerCase();
                    const nomB = b.nom.toLowerCase();

                    if (nomA > nomB) return -1;
                    if (nomA < nomB) return 1;
                    return 0;
                });
                console.log(data);
                productList.innerHTML = "";
                updateList(data)
            })
            prixCroissant.addEventListener("click", () => {
                data.sort((a, b) => {
                    const prixA = a.prix;
                    const prixB = b.prix;

                    if (prixA > prixB) return -1;
                    if (prixA < prixB) return 1;
                    return 0;
                });
                console.log(data);
                productList.innerHTML = "";
                updateList(data)
            })
            prixDecroissant.addEventListener("click", () => {
                data.sort((a, b) => {
                    const prixA = a.prix;
                    const prixB = b.prix;

                    if (prixA < prixB) return -1;
                    if (prixA > prixB) return 1;
                    return 0;
                });
                console.log(data);
                productList.innerHTML = "";
                updateList(data)
            })
        })



    function updateList(arr) {
        arr.forEach(product => {
            let element = document.createElement("li")
            let p1 = document.createElement("p")
            let p2 = document.createElement("p")
            let p3 = document.createElement("p")
            let img = document.createElement("img")
            img.src = product.url
            img.className = "imgProduct"
            p1.textContent = product.nom
            p2.textContent = product.desc
            p3.textContent = product.prix
            element.appendChild(img)
            element.appendChild(p1)
            element.appendChild(p2)
            element.appendChild(p3)
            productList.appendChild(element)

        })
    }



    /** Politique de confidentialité */
    const confidential = localStorage.getItem('confidential');
    const doneConfidential = document.getElementById('doneConfidential');
    const divConfidential = document.getElementById('confidential');


    if (confidential) {
        hiddenConfidential()
    } else {
        localStorage.setItem('confidential', false);
        
    }

    doneConfidential.addEventListener('click', function () {
        localStorage.setItem('confidential', true);
        hiddenConfidential()
    })




    function hiddenConfidential() {
        divConfidential.style.display = "none"
    }
})