<!DOCTYPE html>
<html>
<head>
    <title>Sign In - CryptoMarket</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=AR+One+Sans:wght@400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <div class="header">
        <h1><a href="../index.en.php">CryptoMarket</a></h1>
        <div>
            <a href="login.en.php">Login</a>
            <a href="signin.en.php">Sign In</a>
        </div>
    </div>
    <h1>Registration at CryptoMarket</h1>
    
        <form action="signin.en.php" method="get">
            <label name="email">Mail</label><br>
            <input type="email" name="email">
            
            <br><br>

            <label name="mdp">Password</label><br>
            <input type="password" name="mdp">
            
            <br><br>

            <label name="cmdp">Confirm PassWord</label><br>
            <input type="password" name="cmdp">
            
            <br><br>

            <input type="submit" name="confirmer" value="Sign In">
            
            <br><br>

            <a href="login.en.php">Login</a>
        </form>

<?php

    $email = $_GET['email'];
    $mdp = $_GET['mdp'];
    $cmdp = $_GET['cmdp'];

    if(empty($_GET['email']) || empty($_GET['mdp']) || empty($_GET['cmdp'])){
        echo "Please complete all fields to register.";
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "Enter a valid email address.";
    } else{

    }

    if (strlen($mdp) < 8) {
        echo "Password must contain at least 8 characters.";
    } 
    // Vérification de la présence de lettres majuscules
    elseif (!preg_match("/[A-Z]/", $mdp)) {
        echo "Password must contain at least one capital letter.";
    } 
    // Vérification de la présence de lettres minuscules
    elseif (!preg_match("/[a-z]/", $mdp)) {
        echo "Password must contain at least one lowercase lette.";
    } 
    // Vérification de la présence de chiffres
    elseif (!preg_match("/[0-9]/", $mdp)) {
        echo "Password must contain at least one digit.";
    } 


    if ($cmdp != $mdp){
        echo "Passwords do not match.";
    }else {
        echo "The password is valid.";
        echo "Your account has been created.";
    }

?>

</body>
</html>