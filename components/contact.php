<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Contact - CryptoMarket</title>
</head>
<body>

    <h1>Contactez nous</h1>

    <p>Vous pouvez nous contacter à l'adresse : <a href="mailto:">cryptomarket@gmail.com</a><br>
    Ou par téléphone : <a href="tel:+33">0123456789</a></p>

    <p>Nous sommes disponibles 24h/24 | 7j/7 pour répondre à toutes vos questions et demande.</p><br> 

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d931.8974247889352!2d2.766661871637051!3d48.8537133642692!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e61cd13e858267%3A0x5e4faf9a7b24e6c3!2sIPSSI%20Marne-la-Vall%C3%A9e!5e0!3m2!1sfr!2sfr!4v1696255066209!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe><br>
        
    <form>
        <p>Formulaire de contact</p>
        <label name="email">Email :</label><br>
        <input type="email" name="email"><br><br>
        <label name="demande">Demande :</label><br>
        <textarea name="demande"></textarea>
        <input type="submit" value="Envoyer">
    </form>

    <p>Nous sommes également joigniables sur Linkedin et Instagram<br>
    <a src="www.linkedin.com"><i class="fa fa-linkedin-square"></i></a>
    <a src="www.instagram.com"><i class="fa fa-instagram"></i></a></p>

</body>
</html>