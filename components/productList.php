<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ecommerce</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=AR+One+Sans:wght@400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <div class="header">
        <h1><a href="../index.php">CryptoMarket</a></h1>
        <div>
            <a href="./login.php">Connexion</a>
            <a href="./signin.php">Inscription</a>
        </div>
    </div>
    <div class="container">
        <script src="../script.js"></script>
        <div>
            <button id="A-Z">A-Z</button>
            <button id="Z-A">Z-A</button>
            <button id="prixCroissant">Prix Croissant</button>
            <button id="prixDecroissant">Prix Décroissant</button>
        </div>
        <ul id="productList">
        </ul>
    </div>
</body>
</html>