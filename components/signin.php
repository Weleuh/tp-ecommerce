<!DOCTYPE html>
<html>
<head>
    <title>Inscription - CryptoMarket</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=AR+One+Sans:wght@400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <div class="header">
        <h1><a href="../index.php">CryptoMarket</a></h1>
        <div>
            <a href="login.php">Connexion</a>
            <a href="signin.php">Inscription</a>
        </div>
    </div>
    <h1>Inscription chez CryptoMarket</h1>
    
        <form action="signin.php" method="get">
            <label name="email">Email</label><br>
            <input type="email" name="email">
            
            <br><br>

            <label name="mdp">Mot de passe</label><br>
            <input type="password" name="mdp">
            
            <br><br>

            <label name="cmdp">Confirmer Mot de passe</label><br>
            <input type="password" name="cmdp">
            
            <br><br>

            <input type="submit" name="confirmer" value="Confirmer">
            
            <br><br>

            <a href="login.php">Connexion</a>
        </form>

<?php

    $email = $_GET['email'];
    $mdp = $_GET['mdp'];
    $cmdp = $_GET['cmdp'];

    if(empty($_GET['email']) || empty($_GET['mdp']) || empty($_GET['cmdp'])){
        echo "Veuillez remplir tous les champs pour vous inscrire";
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "Entrez une adresse mail valide.";
    } else{

    }

    if (strlen($mdp) < 8) {
        echo "Le mot de passe doit contenir au moins 8 caractères.";
    } 
    // Vérification de la présence de lettres majuscules
    elseif (!preg_match("/[A-Z]/", $mdp)) {
        echo "Le mot de passe doit contenir au moins une lettre majuscule.";
    } 
    // Vérification de la présence de lettres minuscules
    elseif (!preg_match("/[a-z]/", $mdp)) {
        echo "Le mot de passe doit contenir au moins une lettre minuscule.";
    } 
    // Vérification de la présence de chiffres
    elseif (!preg_match("/[0-9]/", $mdp)) {
        echo "Le mot de passe doit contenir au moins un chiffre.";
    } 


    if ($cmdp != $mdp){
        echo "Les mots de passe ne correspondent pas.";
    }else {
        echo "Le mot de passe est valide.";
        echo "Votre compte à été créé.";
    }

?>

</body>
</html>