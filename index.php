<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ecommerce</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=AR+One+Sans:wght@400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <script src="./script.js"></script>
</head>


<body>
    <div class="header">
        
        <h1>CryptoMarket</h1>
        <div>
            <a href="./components/login.php">Connexion</a>
            <a href="./components/signin.php">Inscription</a>
        </div>
    </div>
    <div id="accueil">
        <img src="logo.png" alt="">
        <h1>Bienvenue sur CryptoMarket !</h1>
        <div>
            <p>Le premier de site d'achat de cryptomonnaie</p>
            <br>
            <p>Vous souhaitez acheter de la cryptomonnaie ? Ce site vous propose les meilleurs prix, avec des mises à jour en temps réels des cours de chaque cryptomonnaie.</p>
        </div>
        <br>
        <div style="text-align: center;">
            <a href="./components/productList.php">
                <button class="btn">Rechercher un produit</button>
            </a>
        </div>
      </div>
      <a href="index.en.php"><img src="EN.jpg" alt="English"></a>
      <a href="index.php"><img src="FR.jpg" alt="Français"></a>

      <div id="confidential">
        <p>bla bla bla cookie confidentiel bla bla</p>
        <button id="doneConfidential">OUI</button>
      </div>
    </div>
    
</body>
</html>