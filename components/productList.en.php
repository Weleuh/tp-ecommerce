<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ecommerce</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=AR+One+Sans:wght@400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../style.css">
</head>
<body>
    <div class="header">
        <h1><a href="../index.en.php">CryptoMarket</a></h1>
        <div>
            <a href="./login.en.php">Login</a>
            <a href="./signin.en.php">Sign In</a>
        </div>
    </div>
    <div class="container">
        <script src="../script.en.js"></script>
        <div>
            <button id="A-Z">A-Z</button>
            <button id="Z-A">Z-A</button>
            <button id="prixCroissant">Ascending price</button>
            <button id="prixDecroissant">Descending Price</button>
        </div>
        <ul id="productList">
        </ul>
    </div>
</body>
</html>