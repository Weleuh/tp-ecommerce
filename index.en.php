<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ecommerce</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=AR+One+Sans:wght@400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <script src="./script.js"></script>
</head>


<body>
    <div class="header">
        <h1>CryptoMarket</h1>
        <div>
            <a href="./components/login.en.php">Login</a>
            <a href="./components/signin.en.php">Sign In</a>
        </div>
    </div>
    <div id="accueil">
        <img src="logo.png" alt="">
        <h1>Welcome on CryptoMarket !</h1>
        <div>
            <p>The first cryptocurrency purchasing site</p>
            <br>
            <p>Do you want to buy cryptocurrency? This site offers you the best prices, with real-time updates of the prices of each cryptocurrency.</p>
        </div>
        <br>
        <div style="text-align: center;">
            <a href="./components/productList.en.php">
                <button class="btn">Search for a product</button>
            </a>
        </div>
      </div>
      <a href="index.en.php"><img src="EN.jpg" alt="English"></a>
      <a href="index.php"><img src="FR.jpg" alt="Français"></a>

      <div id="confidential">
        <p>bla bla bla confidential cookies bla bla</p>
        <button id="doneConfidential">YES</button>
      </div>
    </div>
    
</body>
</html>